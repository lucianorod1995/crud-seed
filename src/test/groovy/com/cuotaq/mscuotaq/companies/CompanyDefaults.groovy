package com.cuotaq.mscuotaq.companies

import com.cuotaq.mscuotaq.companies.adapter.controller.model.CompanyRestModel
import com.cuotaq.mscuotaq.companies.adapter.jdbc.model.CompanyJdbcModel
import com.cuotaq.mscuotaq.companies.domain.Company

class CompanyDefaults {
    public static final int COMPANY_ID = 1121
    public static final String USER_ID = "50a55afb-41ce-44bb-9cfc-d26e60580fa9"
    public static final String NAME = "LuchitoSRL"

    static CompanyRestModel.CompanyRestModelBuilder defaultCompanyRestModel() {
        return CompanyRestModel.builder()
                .companyId(COMPANY_ID)
                .userId(USER_ID)
                .name(NAME)
    }

    static Company.CompanyBuilder defaultCompany() {
        return Company.builder()
                .companyId(COMPANY_ID)
                .userId(USER_ID)
                .name(NAME)
    }

    static CompanyJdbcModel.CompanyJdbcModelBuilder defaultCompanyJdbcModel() {
        return CompanyJdbcModel.builder()
                .companyId(COMPANY_ID)
                .userId(USER_ID)
                .name(NAME)
    }
}
