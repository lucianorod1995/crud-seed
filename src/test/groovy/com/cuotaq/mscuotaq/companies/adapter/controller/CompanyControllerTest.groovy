package com.cuotaq.mscuotaq.companies.adapter.controller

import com.cuotaq.mscuotaq.commons.adapter.controller.model.PageResponse
import com.cuotaq.mscuotaq.commons.application.exception.EntityConflictException
import com.cuotaq.mscuotaq.commons.application.exception.EntityNotFoundException
import com.cuotaq.mscuotaq.commons.application.exception.InvalidFiltersException
import com.cuotaq.mscuotaq.commons.application.exception.RepositoryNotAvailableException
import com.cuotaq.mscuotaq.commons.config.ErrorHandler
import com.cuotaq.mscuotaq.commons.domain.Page
import com.cuotaq.mscuotaq.companies.adapter.controller.model.CompanyRestModel
import com.cuotaq.mscuotaq.companies.adapter.controller.model.CreateCompanyRequestBody
import com.cuotaq.mscuotaq.companies.adapter.controller.model.PatchCompanyRequestBody

import com.cuotaq.mscuotaq.companies.application.port.in.*
import com.cuotaq.mscuotaq.companies.config.ErrorCode

/*IMPORTS*/
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification



import static com.cuotaq.mscuotaq.companies.CompanyDefaults.*
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

class CompanyControllerTest extends Specification {

    static ObjectMapper objectMapper = new ObjectMapper()

    static {
        objectMapper.registerModule(new JavaTimeModule())
    }

    GetCompanyQuery getCompanyQuery = Mock(GetCompanyQuery)
    CreateCompanyCommand createCompanyCommand = Mock(CreateCompanyCommand)
    GetCompaniesQuery getCompaniesQuery = Mock(GetCompaniesQuery)
    PatchCompanyCommand patchCompanyCommand = Mock(PatchCompanyCommand)
    DeleteCompanyCommand deleteCompanyCommand = Mock(DeleteCompanyCommand)

    CompanyController target = new CompanyController(
            getCompanyQuery,
            createCompanyCommand,
            getCompaniesQuery,
            patchCompanyCommand,
            deleteCompanyCommand
    )

    MockMvc mvc = standaloneSetup(target)
            .setControllerAdvice(new ErrorHandler())
            .build()

    def "given a company id when a GET is performed to /api/companies then GetCompanyQuery is executed correctly and the response content is as expected and the response status is 200"() {
        given:
        def data = new GetCompanyQuery.Data(COMPANY_ID)
        def expected = defaultCompanyRestModel().build()
        def aCompany = defaultCompany().build()

        when:
        def response = mvc.perform(get("/api/companies/" + COMPANY_ID))
                .andReturn()
                .response

        then:
        1 * getCompanyQuery.execute(data) >> aCompany
        response.status == 200
        objectMapper.readValue(response.contentAsString, CompanyRestModel) == expected
    }

    def "given an invalid company id when a GET is performed to /api/companies then GetCompanyQuery is not executed and 400 is answered"() {
        when:
        def response = mvc.perform(get("/api/companies/1a"))
                .andReturn()
                .response
        then:
        response.status == 400
        0 * getCompanyQuery.execute(_)
    }

    def "given a company id when a GET is performed to /api/companies and GetCompanyQuery throws {exception} then {status} is answered with the expected message"() {
        given:
        getCompanyQuery.execute(_) >> { throw exception }

        when:
        def response = mvc.perform(get("/api/companies/" + COMPANY_ID))
                .andReturn()
                .response
        then:
        response.status == status
        response.contentAsString.contains(expectedMessage)

        where:
        status | exception                                                                       | expectedMessage
        404    | new EntityNotFoundException(ErrorCode.COMPANY_NOT_FOUND)                        | ErrorCode.COMPANY_NOT_FOUND.name()
        500    | new RuntimeException("Unexpected Error")                                        | ""
        503    | new RepositoryNotAvailableException(ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE) | ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE.name()
    }

    def "given a company when a POST is performed to /api/companies then CreateCompanyCommand is executed correctly and the response content is as expected and the response status is 201"() {
        given:
        def data = new CreateCompanyCommand.Data(USER_ID, NAME)
        def request = new CreateCompanyRequestBody(USER_ID, NAME)
        def expected = defaultCompanyRestModel().build()
        def aCompany = defaultCompany().build()

        when:
        def response = mvc.perform(post("/api/companies/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response

        then:
        1 * createCompanyCommand.execute(data) >> aCompany
        response.status == 201
        objectMapper.readValue(response.contentAsString, CompanyRestModel) == expected
    }

    def "given an invalid request when a POST is performed to /api/companies then CreateCompanyCommand is not executed and 400 is answered with the expected message"() {
        when:
        def response = mvc.perform(post("/api/companies/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response
        then:
        response.status == 400
        objectMapper.readValue(response.contentAsString, Map<String,Object>.class) == expectedMessage
        0 * createCompanyCommand.execute(_)

        where:
        request                                                                     | expectedMessage
        new CreateCompanyRequestBody(null, null)                                    | [user_id: ErrorCode.INVALID_USER_ID.name(), name: ErrorCode.INVALID_NAME.name()]
        /* TODO: add validations here */
    }

    def "given a company when a POST is performed to /api/companies and CreateCompanyCommand throws {exception} then {status} is answered with the expected message"() {
        given:
        def request = new CreateCompanyRequestBody(USER_ID, NAME)
        createCompanyCommand.execute(_) >> { throw exception }

        when:
        def response = mvc.perform(post("/api/companies/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response

        then:
        response.status == status
        response.contentAsString.contains(expectedMessage)

        where:
        status | exception                                                                       | expectedMessage
        409    | new EntityConflictException(ErrorCode.COMPANY_ALREADY_EXISTS)                   | ErrorCode.COMPANY_ALREADY_EXISTS.name()
        500    | new RuntimeException("Unexpected Error")                                        | ""
        503    | new RepositoryNotAvailableException(ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE) | ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE.name()
    }

    def "given a filter when a GET is performed to /api/companies then GetCompaniesListQuery is executed correctly and the response content is as expected and the response status is 200"() {
        given:
        def data = new GetCompaniesQuery.Data([user_id: "equals," + USER_ID])
        def expected = [defaultCompanyRestModel().build()]
        def aCompany = [defaultCompany().build()]

        when:
        def response = mvc.perform(get("/api/companies?user_id=equals," + USER_ID))
                .andReturn()
                .response

        then:
        1 * getCompaniesQuery.execute(data) >> aCompany
        response.status == 200
        objectMapper.readValue(response.contentAsString, new TypeReference<List<CompanyRestModel>>() {}) == expected
    }

    def "given a filter when a GET is performed to /api/companies and GetCompaniesListQuery throws {exception} then {status} is answered with the expected message"() {
        given:
        getCompaniesQuery.execute(_) >> { throw exception }

        when:
        def response = mvc.perform(get("/api/companies/"))
                .andReturn()
                .response

        then:
        response.status == status
        response.contentAsString.contains(expectedMessage)


        where:
        status | exception                                                                                 | expectedMessage
        400    | new InvalidFiltersException(com.cuotaq.mscuotaq.commons.config.ErrorCode.INVALID_FILTERS) | com.cuotaq.mscuotaq.commons.config.ErrorCode.INVALID_FILTERS.name()
        500    | new RuntimeException("Unexpected Error")                                                  | ""
        503    | new RepositoryNotAvailableException(ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE)           | ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE.name()
    }

    def "given a filter and a pagination when a GET is performed to /api/companies then getCompaniesQuery is executed correctly and the response content is as expected and the response status is 200"() {
        given:
        def data = new GetCompaniesQuery.Data([user_id: "equals," + USER_ID])
        def expected = PageResponse.builder()
                .content([defaultCompanyRestModel().build()])
                .totalElements(1)
                .size(1)
                .number(1)
                .totalPages(1)
                .build()
        def aCompany = Page.builder()
                .content([defaultCompany().build()])
                .totalElements(1)
                .size(1)
                .number(1)
                .totalPages(1)
                .build()

        when:
        def response = mvc.perform(get("/api/companies?user_id=equals," + USER_ID + "&page=0&size=10"))
                .andReturn()
                .response

        then:
        1 * getCompaniesQuery.execute(data, 0, 10) >> aCompany
        response.status == 200
        objectMapper.readValue(response.contentAsString, new TypeReference<PageResponse<CompanyRestModel>>() {
        }) == expected
    }

    def "given a filter and a pagination when a GET is performed to /api/companies and getCompaniesQuery throws {exception} then {status} is answered with the expected message"() {
        given:
        getCompaniesQuery.execute(_, _, _) >> { throw exception }

        when:
        def response = mvc.perform(get("/api/companies?user_id=equals," + USER_ID + "&page=0&size=10"))
                .andReturn()
                .response

        then:
        response.status == status
        response.contentAsString.contains(expectedMessage)

        where:
        status | exception                                                                                 | expectedMessage
        400    | new InvalidFiltersException(com.cuotaq.mscuotaq.commons.config.ErrorCode.INVALID_FILTERS) | com.cuotaq.mscuotaq.commons.config.ErrorCode.INVALID_FILTERS.name()
        500    | new RuntimeException("Unexpected Error")                                                  | ""
        503    | new RepositoryNotAvailableException(ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE)           | ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE.name()
    }

    def "given a company and a company id when a PATCH is performed to /api/companies then PatchCompanyCommand is executed correctly and the response content is as expected and the response status is 200"() {
        given:
        def data = new PatchCompanyCommand.Data(COMPANY_ID, USER_ID, NAME)
        def request = new PatchCompanyRequestBody(USER_ID, NAME)
        def expected = defaultCompanyRestModel().build()
        def aCompany = defaultCompany().build()

        when:
        def response = mvc.perform(patch("/api/companies/" + COMPANY_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response

        then:
        1 * patchCompanyCommand.execute(data) >> aCompany
        response.status == 200
        objectMapper.readValue(response.contentAsString, CompanyRestModel) == expected
    }

    def "given an invalid company id when a PATCH is performed to /api/companies then PatchCompanyQuery is not executed and 400 is answered"() {
        when:
        def response = mvc.perform(patch("/api/companies/1a"))
                .andReturn()
                .response
        then:
        response.status == 400
        0 * patchCompanyCommand.execute(_)
    }

    /* TODO: add validations here
    def "given an invalid request when a PATCH is performed to /api/companies then PatchCompanyCommand is not executed and 400 is answered with the expected message"() {
        when:
        def response = mvc.perform(patch("/api/companies/" + COMPANY_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response
        then:
        response.status == 400
        response.contentAsString.contains(expectedMessage)
        0 * patchCompanyCommand.execute(_)

        where:
        request                                                                    | expectedMessage
        new PatchCompanyRequestBody("16a5dd32-3d2a-4aa5-9980-6b3e2de305a", null)   | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new PatchCompanyRequestBody("16a5dd32-3d2a-4aa5-9980-6b3e2de305a11", NAME) | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new PatchCompanyRequestBody("16a5dd32-3d2a-4aa5-9980-6b3e2de305g0", NAME)  | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new PatchCompanyRequestBody("16a5dd323d2a4aa599806b3e2de305a", NAME)       | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new PatchCompanyRequestBody(null, "AB")                                    | objectMapper.writeValueAsString([name: ErrorCode.INVALID_NAME.name()])
        new PatchCompanyRequestBody(USER_ID, "A" * 256)                            | objectMapper.writeValueAsString([name: ErrorCode.INVALID_NAME.name()])
    }*/

    def "given a company when a PATCH is performed to /api/companies and PatchCompanyCommand throws {exception} then {status} is answered with the expected message"() {
        given:
        def request = new PatchCompanyRequestBody(USER_ID, NAME)
        patchCompanyCommand.execute(_) >> { throw exception }

        when:
        def response = mvc.perform(patch("/api/companies/" + COMPANY_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response

        then:
        response.status == status
        response.contentAsString.contains(expectedMessage)

        where:
        status | exception                                                                       | expectedMessage
        404    | new EntityNotFoundException(ErrorCode.COMPANY_NOT_FOUND)                        | ErrorCode.COMPANY_NOT_FOUND.name()
        409    | new EntityConflictException(ErrorCode.COMPANY_ALREADY_EXISTS)                   | ErrorCode.COMPANY_ALREADY_EXISTS.name()
        500    | new RuntimeException("Unexpected Error")                                        | ""
        503    | new RepositoryNotAvailableException(ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE) | ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE.name()
    }

    def "given a company id when a DELETE is performed to /api/companies then DeleteCompanyCommand is executed correctly and the response content is as expected and the response status is 204"() {
        given:
        def data = new DeleteCompanyCommand.Data(COMPANY_ID)

        when:
        def response = mvc.perform(delete("/api/companies/" + COMPANY_ID))
                .andReturn()
                .response

        then:
        1 * deleteCompanyCommand.execute(data)
        response.status == 204
    }

    def "given an invalid company id when a DELETE is performed to /api/companies then DeleteCompanyCommand is not executed and 400 is answered"() {
        when:
        def response = mvc.perform(delete("/api/companies/1a"))
                .andReturn()
                .response
        then:
        response.status == 400
        0 * deleteCompanyCommand.execute(_)
    }

    def "given a company id when a DELETE is performed to /api/companies and DeleteCompanyCommand throws {exception} then {status} is answered with the expected message"() {
        given:
        deleteCompanyCommand.execute(_) >> { throw exception }

        when:
        def response = mvc.perform(delete("/api/companies/" + COMPANY_ID))
                .andReturn()
                .response
        then:
        response.status == status
        response.contentAsString.contains(expectedMessage)

        where:
        status | exception                                                                       | expectedMessage
        500    | new RuntimeException("Unexpected Error")                                        | ""
        503    | new RepositoryNotAvailableException(ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE) | ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE.name()
    }
}
