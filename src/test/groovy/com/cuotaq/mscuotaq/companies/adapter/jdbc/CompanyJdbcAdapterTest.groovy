package com.cuotaq.mscuotaq.companies.adapter.jdbc

import com.cuotaq.mscuotaq.commons.application.exception.EntityConflictException
import com.cuotaq.mscuotaq.commons.application.exception.EntityNotFoundException
import com.cuotaq.mscuotaq.commons.application.exception.RepositoryNotAvailableException
import com.cuotaq.mscuotaq.commons.domain.Page
import com.cuotaq.mscuotaq.companies.adapter.jdbc.model.CompanyJdbcModel
import com.cuotaq.mscuotaq.companies.domain.Company
/*IMPORTS*/
import org.springframework.dao.DataAccessException
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import spock.lang.Specification



import static com.cuotaq.mscuotaq.companies.CompanyDefaults.*

class CompanyJdbcAdapterTest extends Specification {

    NamedParameterJdbcTemplate jdbcTemplate = Mock(NamedParameterJdbcTemplate)

    CompanyJdbcAdapter target = new CompanyJdbcAdapter(jdbcTemplate)

    def "given a company creation data when save is executed then jdbcTemplate is called correctly and response is as expected"() {
        given:
        def aCompanyToCreate = defaultCompany().companyId(null).build()
        def expected = defaultCompany().build()

        when:
        def response = target.save(aCompanyToCreate)

        then:
        1 * jdbcTemplate.queryForObject({ it.contains("INSERT INTO companies") }, ["USER_ID": USER_ID, "NAME": NAME], Integer.class) >> COMPANY_ID
        response == expected
    }

    def "given a company creation data when save is executed and the jdbcTemplate throws {exception} then {expected} is thrown"() {
        given:
        def aCompanyToCreate = defaultCompany().companyId(null).build()
        jdbcTemplate.queryForObject(_, _, _) >> { throw exception }

        when:
        target.save(aCompanyToCreate)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityConflictException         | Mock(DataIntegrityViolationException)
        RepositoryNotAvailableException | Mock(DataAccessException)
        RuntimeException                | new Exception()
    }

    def "given a company id when deleteById is executed then jdbcTemplate is called correctly"() {
        when:
        target.deleteById(COMPANY_ID)

        then:
        1 * jdbcTemplate.update({ it.contains("DELETE FROM companies") }, ["COMPANY_ID": COMPANY_ID])
    }

    def "given a company id when deleteById is executed and the jdbcTemplate throws {exception} then {expected} is thrown"() {
        given:
        jdbcTemplate.update(_, _) >> { throw exception }

        when:
        target.deleteById(COMPANY_ID)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityNotFoundException         | Mock(EmptyResultDataAccessException)
        RepositoryNotAvailableException | Mock(DataAccessException)
        RuntimeException                | new Exception()
    }

    def "given companies and its jdbcModel when findAll is executed then jdbcTemplate is called correctly and response is as expected"() {
        given:
        def aCompany = defaultCompany().build()
        def otherCompany = defaultCompany().build()
        def aCompanyJdbcModel = defaultCompanyJdbcModel().build()
        def otherCompanyJdbcModel = defaultCompanyJdbcModel().build()
        def expected = [aCompany, otherCompany]

        when:
        def response = target.findAll(Collections.emptyList())

        then:
        1 * jdbcTemplate.query({ it.contains("SELECT * FROM companies") }, Collections.emptyMap(), _) >> [aCompanyJdbcModel, otherCompanyJdbcModel]
        response == expected
    }

    def "given any filters when findAll is executed and the jdbcTemplate throws {exception} then {expected} is thrown"() {
        given:
        jdbcTemplate.query(_, _, _) >> { throw exception }

        when:
        target.findAll(Collections.emptyList())

        then:
        thrown(expected)

        where:
        expected                        | exception
        RepositoryNotAvailableException | Mock(DataAccessException)
        RuntimeException                | new Exception()
    }

    def "given a pagination, companies and its jdbcModel when findAll is executed then jdbcTemplate is called correctly and response is as expected"() {
        given:
        def aCompany = defaultCompany().build()
        def otherCompany = defaultCompany().build()
        def aCompanyJdbcModel = defaultCompanyJdbcModel().build()
        def otherCompanyJdbcModel = defaultCompanyJdbcModel().build()
        def expected = Page.builder()
                .content([aCompany, otherCompany])
                .totalElements(2)
                .size(10)
                .number(0)
                .totalPages(1)
                .build()

        when:
        def response = target.findAll(0, 10, Collections.emptyList())

        then:
        1 * jdbcTemplate.queryForObject({ it.contains("SELECT COUNT(*) FROM (SELECT * FROM companies") }, Collections.emptyMap(), Integer.class) >> 2
        1 * jdbcTemplate.query({ it.contains("SELECT * FROM companies") }, Collections.emptyMap(), _) >> [aCompanyJdbcModel, otherCompanyJdbcModel]
        response == expected
    }

    def "given any pagination and filters when findAll is executed and the jdbcTemplate throws {exception} then {expected} is thrown"() {
        given:
        jdbcTemplate.query(_, _, _) >> { throw exception }

        when:
        target.findAll(0, 10, Collections.emptyList())

        then:
        thrown(expected)
        1 * jdbcTemplate.queryForObject({ it.contains("SELECT COUNT(*) FROM (SELECT * FROM companies") }, Collections.emptyMap(), Integer.class) >> 2

        where:
        expected                        | exception
        RepositoryNotAvailableException | Mock(DataAccessException)
        RuntimeException                | new Exception()
    }

    def "given a company id when findById is executed then jdbcTemplate is called correctly and response is as expected"() {
        given:
        def aCompanyJdbcModel = defaultCompanyJdbcModel().build()
        def expected = defaultCompany().build()

        when:
        def response = target.findById(COMPANY_ID)

        then:
        1 * jdbcTemplate.queryForObject({ it.contains("SELECT * FROM companies") }, ["COMPANY_ID": COMPANY_ID], _) >> aCompanyJdbcModel
        response == expected
    }

    def "given a company id when findById is executed and the jdbcTemplate throws {exception} then {expected} is thrown"() {
        given:
        jdbcTemplate.queryForObject(_, _, _) >> { throw exception }

        when:
        target.findById(COMPANY_ID)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityNotFoundException         | Mock(EmptyResultDataAccessException)
        RepositoryNotAvailableException | Mock(DataAccessException)
        RuntimeException                | new Exception()
    }

    def "given a company patch data when patch is executed then jdbcTemplate is called correctly and response is as expected"() {
        given:
        def aCompanyToPatch = defaultCompany().name(null).build()
        def aCompanyJdbcModel = defaultCompanyJdbcModel().build()
        def expected = defaultCompany().build()

        when:
        def response = target.patch(aCompanyToPatch)

        then:
        1 * jdbcTemplate.queryForObject({ it.contains("UPDATE companies SET") }, ["COMPANY_ID": COMPANY_ID, "USER_ID": USER_ID, "NAME": null], _) >> aCompanyJdbcModel
        response == expected
    }

    def "given a company patch data when patch is executed and the jdbcTemplate throws {exception} then {expected} is thrown"() {
        given:
        def aCompanyToPatch = defaultCompany().name(null).build()
        jdbcTemplate.queryForObject(_, _, _) >> { throw exception }

        when:
        target.patch(aCompanyToPatch)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityNotFoundException         | Mock(EmptyResultDataAccessException)
        EntityConflictException         | Mock(DataIntegrityViolationException)
        RepositoryNotAvailableException | Mock(DataAccessException)
        RuntimeException                | new Exception()
    }
}
