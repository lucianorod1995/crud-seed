package com.cuotaq.mscuotaq.companies.application.usecase

import com.cuotaq.mscuotaq.commons.application.exception.EntityConflictException
import com.cuotaq.mscuotaq.commons.application.exception.EntityNotFoundException
import com.cuotaq.mscuotaq.commons.application.exception.RepositoryNotAvailableException
import com.cuotaq.mscuotaq.companies.application.port.in.PatchCompanyCommand
import com.cuotaq.mscuotaq.companies.application.port.out.CompanyRepository
import spock.lang.Specification

/*IMPORTS*/

import static com.cuotaq.mscuotaq.companies.CompanyDefaults.*



class PatchCompanyUseCaseTest extends Specification {

    CompanyRepository companyRepository = Mock(CompanyRepository)

    PatchCompanyUseCase target = new PatchCompanyUseCase(companyRepository)

    def "given a company patch data when PatchCompanyUseCase is executed then repository is called correctly and response is as expected"() {
        given:
        def data = new PatchCompanyCommand.Data(COMPANY_ID, USER_ID, null)
        def aCompanyToPatch = defaultCompany().name(null).build()
        def expected = defaultCompany().build()

        when:
        def response = target.execute(data)

        then:
        1 * companyRepository.patch(aCompanyToPatch) >> expected
        response == expected
    }

    def "given a company patch data when PatchCompanyUseCase is executed and the repository throws {exception} then {expected} is thrown"() {
        given:
        def data = new PatchCompanyCommand.Data(COMPANY_ID, USER_ID, null)
        companyRepository.patch(_) >> { throw exception }

        when:
        target.execute(data)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityNotFoundException         | new EntityNotFoundException()
        EntityConflictException         | new EntityConflictException()
        RepositoryNotAvailableException | new RepositoryNotAvailableException()
        Exception                       | new Exception()
    }
}
