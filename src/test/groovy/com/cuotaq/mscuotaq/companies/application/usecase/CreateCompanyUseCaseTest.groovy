package com.cuotaq.mscuotaq.companies.application.usecase

import com.cuotaq.mscuotaq.commons.application.exception.EntityConflictException
import com.cuotaq.mscuotaq.commons.application.exception.RepositoryNotAvailableException
import com.cuotaq.mscuotaq.companies.application.port.in.CreateCompanyCommand
import com.cuotaq.mscuotaq.companies.application.port.out.CompanyRepository
import spock.lang.Specification

/*IMPORTS*/


import static com.cuotaq.mscuotaq.companies.CompanyDefaults.*

class CreateCompanyUseCaseTest extends Specification {

    CompanyRepository companyRepository = Mock(CompanyRepository)

    CreateCompanyUseCase target = new CreateCompanyUseCase(companyRepository)

    def "given a company creation data when CreateCompanyUseCase is executed then repository is called correctly and response is as expected"() {
        given:
        def data = new CreateCompanyCommand.Data(USER_ID, NAME)
        def aCompanyToCreate = defaultCompany().companyId(null).build()
        def expected = defaultCompany().build()

        when:
        def response = target.execute(data)

        then:
        1 * companyRepository.save(aCompanyToCreate) >> expected
        response == expected
    }

    def "given a company creation data when CreateCompanyUseCase is executed and the repository throws {exception} then {expected} is thrown"() {
        given:
        def data = new CreateCompanyCommand.Data(USER_ID, NAME)
        companyRepository.save(_) >> { throw exception }

        when:
        target.execute(data)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityConflictException         | new EntityConflictException()
        RepositoryNotAvailableException | new RepositoryNotAvailableException()
        Exception                       | new Exception()
    }
}
