package com.cuotaq.mscuotaq.companies.application.usecase

import com.cuotaq.mscuotaq.commons.application.exception.EntityNotFoundException
import com.cuotaq.mscuotaq.commons.application.exception.RepositoryNotAvailableException
import com.cuotaq.mscuotaq.companies.application.port.in.GetCompanyQuery
import com.cuotaq.mscuotaq.companies.application.port.out.CompanyRepository
import spock.lang.Specification

/*IMPORTS*/


import static com.cuotaq.mscuotaq.companies.CompanyDefaults.*

class GetCompanyUseCaseTest extends Specification {

    CompanyRepository companyRepository = Mock(CompanyRepository)

    GetCompanyUseCase target = new GetCompanyUseCase(companyRepository)

    def "given a company id when GetCompanyUseCase is executed then repository is called correctly and response is as expected"() {
        given:
        def data = new GetCompanyQuery.Data(COMPANY_ID)
        def expected = defaultCompany().build()

        when:
        def response = target.execute(data)

        then:
        1 * companyRepository.findById(COMPANY_ID) >> expected
        response == expected
    }

    def "given any filters when GetCompanyUseCase is executed and the repository throws {exception} then {expected} is thrown"() {
        given:
        def data = new GetCompanyQuery.Data(COMPANY_ID)
        companyRepository.findById(_) >> { throw exception }

        when:
        target.execute(data)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityNotFoundException         | new EntityNotFoundException()
        RepositoryNotAvailableException | new RepositoryNotAvailableException()
        Exception                       | new Exception()
    }
}
