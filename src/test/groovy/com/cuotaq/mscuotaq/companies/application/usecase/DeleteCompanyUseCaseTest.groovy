package com.cuotaq.mscuotaq.companies.application.usecase

import com.cuotaq.mscuotaq.commons.application.exception.EntityNotFoundException
import com.cuotaq.mscuotaq.commons.application.exception.RepositoryNotAvailableException
import com.cuotaq.mscuotaq.companies.application.port.in.DeleteCompanyCommand
import com.cuotaq.mscuotaq.companies.application.port.out.CompanyRepository
/*IMPORTS*/
import spock.lang.Specification



import static com.cuotaq.mscuotaq.companies.CompanyDefaults.*

class DeleteCompanyUseCaseTest extends Specification {

    CompanyRepository companyRepository = Mock(CompanyRepository)

    DeleteCompanyUseCase target = new DeleteCompanyUseCase(companyRepository)

    def "given a company id when DeleteCompanyUseCase is executed then repository is called correctly"() {
        given:
        def data = new DeleteCompanyCommand.Data(COMPANY_ID)

        when:
        target.execute(data)

        then:
        1 * companyRepository.deleteById(COMPANY_ID)
    }

    def "given a company id when DeleteCompanyUseCase is executed and the repository throws EntityNotFoundException then no exception is thrown"() {
        given:
        def data = new DeleteCompanyCommand.Data(COMPANY_ID)
        companyRepository.deleteById(_) >> { throw new EntityNotFoundException() }

        when:
        target.execute(data)

        then:
        noExceptionThrown()
    }

    def "given a company id when DeleteCompanyUseCase is executed and the repository throws {exception} then {expected} is thrown"() {
        given:
        def data = new DeleteCompanyCommand.Data(COMPANY_ID)
        companyRepository.deleteById(_) >> { throw exception }

        when:
        target.execute(data)

        then:
        thrown(expected)

        where:
        expected                        | exception
        RepositoryNotAvailableException | new RepositoryNotAvailableException()
        Exception                       | new Exception()
    }
}
