package com.cuotaq.mscuotaq.companies.application.usecase

import com.cuotaq.mscuotaq.commons.application.exception.RepositoryNotAvailableException
import com.cuotaq.mscuotaq.commons.domain.Page
import com.cuotaq.mscuotaq.commons.domain.filters.EqualsFilter
import com.cuotaq.mscuotaq.companies.application.port.in.GetCompaniesQuery
import com.cuotaq.mscuotaq.companies.application.port.out.CompanyRepository
import spock.lang.Specification

/*IMPORTS*/


import static com.cuotaq.mscuotaq.companies.CompanyDefaults.*

class GetCompaniesUseCaseTest extends Specification {

    CompanyRepository companyRepository = Mock(CompanyRepository)

    GetCompaniesUseCase target = new GetCompaniesUseCase(companyRepository)

    def "given the filters {queryParams} when GetCompaniesListUseCase is executed then repository is called correctly and response is as expected"() {
        given:
        def data = new GetCompaniesQuery.Data(queryParams)
        def aCompany = defaultCompany().build()
        def otherCompany = defaultCompany().build()
        def expected = [aCompany, otherCompany]

        when:
        def response = target.execute(data)

        then:
        1 * companyRepository.findAll(filters) >> expected
        response == expected

        where:
        queryParams                    | filters
        Collections.emptyMap()         | Collections.emptyList()
        [user_id: "equals," + USER_ID] | [new EqualsFilter("user_id", USER_ID)]
    }

    def "given any filters when GetCompaniesListUseCase is executed and the repository throws {exception} then {expected} is thrown"() {
        given:
        def data = new GetCompaniesQuery.Data(Collections.emptyMap())
        companyRepository.findAll(_) >> { throw exception }

        when:
        target.execute(data)

        then:
        thrown(expected)

        where:
        expected                        | exception
        RepositoryNotAvailableException | new RepositoryNotAvailableException()
        Exception                       | new Exception()
    }

    def "given the filters {queryParams} when GetCompaniesPageUseCase is executed then repository is called correctly and response is as expected"() {
        given:
        def data = new GetCompaniesQuery.Data(queryParams as Map<String, String>)
        def aCompany = defaultCompany().build()
        def expected = Page.builder()
                .content([aCompany])
                .totalElements(1)
                .size(1)
                .number(1)
                .totalPages(1)
                .build()

        when:
        def response = target.execute(data, 0, 10)

        then:
        1 * companyRepository.findAll(0, 10, filters) >> expected
        response == expected

        where:
        queryParams                    | filters
        Collections.emptyMap()         | Collections.emptyList()
        [user_id: "equals," + USER_ID] | [new EqualsFilter("user_id", USER_ID)]
    }

    def "given any filters when GetCompaniesPageUseCase is executed and the repository throws {exception} then {expected} is thrown"() {
        given:
        def data = new GetCompaniesQuery.Data(Collections.emptyMap())
        companyRepository.findAll(_, _, _) >> { throw exception }

        when:
        target.execute(data, 0, 10)

        then:
        thrown(expected)

        where:
        expected                        | exception
        RepositoryNotAvailableException | new RepositoryNotAvailableException()
        Exception                       | new Exception()
    }

}
