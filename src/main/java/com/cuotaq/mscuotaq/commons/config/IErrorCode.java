package com.cuotaq.mscuotaq.commons.config;

public interface IErrorCode {
    String name();
}
