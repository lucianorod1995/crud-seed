package com.cuotaq.mscuotaq.commons.config;

import com.cuotaq.mscuotaq.commons.application.exception.BusinessException;
import com.cuotaq.mscuotaq.commons.application.exception.EntityConflictException;
import com.cuotaq.mscuotaq.commons.application.exception.EntityNotFoundException;
import com.cuotaq.mscuotaq.commons.application.exception.ForbiddenException;
import com.cuotaq.mscuotaq.commons.application.exception.RepositoryNotAvailableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Order(Ordered.LOWEST_PRECEDENCE)
@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler({BusinessException.class})
    public ResponseEntity handle(BusinessException ex) {
        String errorName = ex.getErrorCode().name();
        log.error(errorName, ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(errorName);
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity handle(MethodArgumentTypeMismatchException ex) {
        log.error("Bad argument", ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .build();
    }

    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ResponseEntity handle(HttpMessageNotReadableException ex) {
        log.error("Bad argument", ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(ex.getMessage());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity handle(MethodArgumentNotValidException ex) {
        log.error("Bad argument", ex);
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(camelToSnake(fieldName), errorMessage);
        });
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(errors);
    }
    @ExceptionHandler({ForbiddenException.class})
    public ResponseEntity handle(ForbiddenException ex) {
        String errorName = ex.getErrorCode().name();
        log.error(errorName, ex);
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(errorName);
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity handle(EntityNotFoundException ex) {
        String errorName = ex.getErrorCode().name();
        log.error(errorName, ex);
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
            .body(errorName);
    }

    @ExceptionHandler({EntityConflictException.class})
    public ResponseEntity handle(EntityConflictException ex) {
        String errorName = ex.getErrorCode().name();
        log.error(errorName, ex);
        return ResponseEntity.status(HttpStatus.CONFLICT)
            .body(errorName);
    }

    @ExceptionHandler({RepositoryNotAvailableException.class})
    public ResponseEntity handle(RepositoryNotAvailableException ex) {
        String errorName = ex.getErrorCode().name();
        log.error(errorName, ex);
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
            .body(errorName);
    }

    @ExceptionHandler({IOException.class})
    public ResponseEntity handle(IOException ex) {
        log.error("Error during IO operation", ex);
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                .body(ex.getMessage());
    }

    @ExceptionHandler({Throwable.class})
    public ResponseEntity handle(Throwable ex) {
        log.error("Unexpected Error", ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    private static String camelToSnake(String str) {
        return str.replaceAll("([a-z])([A-Z]+)", "$1_$2").toLowerCase();
    }
}

