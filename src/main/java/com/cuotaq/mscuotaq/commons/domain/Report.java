package com.cuotaq.mscuotaq.commons.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Getter
@Data
@Builder
@AllArgsConstructor
public class Report {
    String filename;
    byte[] file;

    public String getName() {
        return filename;
    }
}
