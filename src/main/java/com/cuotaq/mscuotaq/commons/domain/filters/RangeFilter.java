package com.cuotaq.mscuotaq.commons.domain.filters;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class RangeFilter extends Filter {

    String key;
    String from;
    String to;
    @Builder.Default
    boolean fromIncluded = true;
    @Builder.Default
    boolean toIncluded = true;

    public RangeFilter(String key, String from, String to) {
        this.key = key;
        this.from = from;
        this.to = to;
    }

    //range,value1
    //range,value1,value2
    //range,,value2
    //range,value1,false,value2,false
    @Override
    public Filter build(String key, List<String> values) {
        RangeFilterBuilder builder = new RangeFilterBuilder()
                .key(key)
                .from(getValue(values.get(0)));

        if (values.size() >= 4) {
            String fromIncludedStr = getValue(values.get(1));
            String toIncludedStr = getValue(values.get(3));
            builder = builder.fromIncluded(Boolean.parseBoolean(fromIncludedStr))
                    .to(getValue(values.get(2)))
                    .toIncluded(Boolean.parseBoolean(toIncludedStr));
        } else {
            builder = builder.to(values.size() > 1 ? getValue(values.get(1)) : null);
        }
        return builder.build();
    }

    private String getValue(String value) {
        return StringUtils.hasText(value) ? value : null;
    }
}
