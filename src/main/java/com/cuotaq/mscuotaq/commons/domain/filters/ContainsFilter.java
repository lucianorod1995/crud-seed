package com.cuotaq.mscuotaq.commons.domain.filters;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContainsFilter extends Filter {

    String key;
    String value;

    @Override
    public Filter build(String key, List<String> values) {
        return ContainsFilter.builder()
                .key(key)
                .value(values == null || values.isEmpty() ? null : values.get(0))
                .build();
    }
}
