package com.cuotaq.mscuotaq.commons.domain;

import lombok.Builder;
import lombok.Value;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Builder
@Value
public class Page<T> {
    List<T> content;
    long totalElements;
    int size;
    int number;
    int totalPages;

    public <R> Page<R> map(Function<T, R> function) {
        List<R> newContent = content.stream()
                .map(function)
                .collect(Collectors.toList());

        return Page.<R>builder()
                .content(newContent)
                .totalElements(this.totalElements)
                .size(this.size)
                .number(this.number)
                .totalPages(this.totalPages)
                .build();
    }
}
