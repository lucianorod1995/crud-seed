package com.cuotaq.mscuotaq.commons.domain.filters;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper=false)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotEqualsFilter extends Filter {

    String key;
    String value;

    @Override
    public Filter build(String key, List<String> values) {
        return new NotEqualsFilterBuilder()
                .key(key)
                .value(values.get(0))
                .build();
    }
}
