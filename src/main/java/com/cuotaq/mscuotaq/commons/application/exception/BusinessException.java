package com.cuotaq.mscuotaq.commons.application.exception;

import com.cuotaq.mscuotaq.commons.config.IErrorCode;

public class BusinessException extends ErrorCodeException {

    public BusinessException(IErrorCode errorCode) {
        super(errorCode);
    }
}
