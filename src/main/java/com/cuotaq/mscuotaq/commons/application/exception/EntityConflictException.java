package com.cuotaq.mscuotaq.commons.application.exception;

import com.cuotaq.mscuotaq.commons.config.IErrorCode;

public class EntityConflictException extends ErrorCodeException {

    public EntityConflictException(IErrorCode errorCode) {
        super(errorCode);
    }
}
