package com.cuotaq.mscuotaq.commons.application.exception;

import com.cuotaq.mscuotaq.commons.config.IErrorCode;

public class InternalErrorException  extends ErrorCodeException {

    public InternalErrorException(IErrorCode errorCode) {
        super(errorCode);
    }
}