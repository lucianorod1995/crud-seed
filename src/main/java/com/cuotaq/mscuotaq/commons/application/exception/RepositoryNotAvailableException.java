package com.cuotaq.mscuotaq.commons.application.exception;

import com.cuotaq.mscuotaq.commons.config.IErrorCode;

public class RepositoryNotAvailableException extends ErrorCodeException {

    public RepositoryNotAvailableException(IErrorCode errorCode) {
        super(errorCode);
    }
}
