package com.cuotaq.mscuotaq.commons.application.exception;

import com.cuotaq.mscuotaq.commons.config.IErrorCode;

public class ForbiddenException extends ErrorCodeException {

    public ForbiddenException(IErrorCode errorCode) {
        super(errorCode);
    }
}
