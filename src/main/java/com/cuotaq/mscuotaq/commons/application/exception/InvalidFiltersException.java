package com.cuotaq.mscuotaq.commons.application.exception;

import com.cuotaq.mscuotaq.commons.config.ErrorCode;

public class InvalidFiltersException extends BusinessException {
    public InvalidFiltersException(ErrorCode errorCode) {
        super(errorCode);
    }
}