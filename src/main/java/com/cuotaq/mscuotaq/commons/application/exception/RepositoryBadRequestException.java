package com.cuotaq.mscuotaq.commons.application.exception;

import com.cuotaq.mscuotaq.commons.config.IErrorCode;

public class RepositoryBadRequestException extends ErrorCodeException {

    public RepositoryBadRequestException(IErrorCode errorCode) {
        super(errorCode);
    }
}
