package com.cuotaq.mscuotaq.commons.application.exception;

import com.cuotaq.mscuotaq.commons.config.IErrorCode;

public class EntityNotFoundException extends ErrorCodeException {

    public EntityNotFoundException(IErrorCode errorCode) {
        super(errorCode);
    }
}
