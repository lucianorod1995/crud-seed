package com.cuotaq.mscuotaq.commons.application.exception;


import com.cuotaq.mscuotaq.commons.config.IErrorCode;

public abstract class ErrorCodeException extends RuntimeException {

    private final IErrorCode errorCode;

    public ErrorCodeException(IErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public IErrorCode getErrorCode() {
        return this.errorCode;
    }
}
