package com.cuotaq.mscuotaq.commons.application.exception;

import com.cuotaq.mscuotaq.commons.config.IErrorCode;

public class RepositoryNotFoundException extends ErrorCodeException {

    public RepositoryNotFoundException(IErrorCode errorCode) {
        super(errorCode);
    }
}
