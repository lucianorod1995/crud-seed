package com.cuotaq.mscuotaq.commons.adapter.jbdc.exception;

public class SqlReaderException extends RuntimeException {
    public SqlReaderException(String message) {
        super(message);
    }

    public SqlReaderException(Throwable cause) {
        super(cause);
    }
}
