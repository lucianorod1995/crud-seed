package com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types;

import com.cuotaq.mscuotaq.commons.domain.filters.ContainsFilter;
import com.cuotaq.mscuotaq.commons.domain.filters.Filter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JdbcContainsFilter implements JdbcFilter {

    private String field;
    private Object value;

    @Override
    public String getConditionQuery() {
        if (value == null || value.toString().isEmpty()) {
            return field + " iLIKE '%'";
        }
        return field + " iLIKE '%" + value.toString() + "%'";
    }

    @Override
    public JdbcFilter fromDomain(Filter domainFilter, Class<?> clazz) {
        ContainsFilter domain = (ContainsFilter) domainFilter;
        return JdbcContainsFilter.builder()
                .field(domain.getKey())
                .value(domain.getValue())
                .build();
    }

}
