package com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types;


import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.formatters.JdbcFormatter;
import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.formatters.JdbcNoFormatter;
import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.formatters.JdbcStringFormatter;
import com.cuotaq.mscuotaq.commons.domain.filters.Filter;
import com.cuotaq.mscuotaq.commons.utils.StringUtils;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.Map;

public interface JdbcFilter {

    JdbcFormatter defaultFormatter = new JdbcNoFormatter();
    JdbcFormatter stringFormatter = new JdbcStringFormatter();

    Map<Class<?>, ? extends JdbcFormatter> formatters = Map.of(
            String.class, stringFormatter,
            LocalDateTime.class, stringFormatter
    );

    String getConditionQuery();

    JdbcFilter fromDomain(Filter domain, Class<?> jdbcModelClass);

    static JdbcFormatter getFormatter(Class<?> clazz, String key) {
        if(clazz == null) {
            return defaultFormatter;
        }
        Field field;
        for (String name : key.split("\\.")) {
            try {
                field = clazz.getDeclaredField(StringUtils.snakeToCamel(name));
            } catch (NoSuchFieldException e) {
                return defaultFormatter;
            }
            clazz = field.getType();
        }
        return formatters.containsKey(clazz) ? formatters.get(clazz) : defaultFormatter;
    }
}
