package com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters;

import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types.JdbcFilter;
import org.springframework.util.StringUtils;

import java.util.List;

public class JdbcQueryBuilder {

    public static String buildCountQuery(String query, List<JdbcFilter> filters) {
        StringBuilder sb = appendFilters(query, filters);

        return "SELECT COUNT(*) FROM (" + sb + ") aux";
    }

    public static String buildSelectQuery(String query, List<JdbcFilter> filters) {
        StringBuilder sb = appendFilters(query, filters);
        return sb.toString();
    }

    public static String buildPagedSelectQuery(String query, String orderFieldName, List<JdbcFilter> filters, int page, int size) {
        StringBuilder sb = appendFilters(query, filters);
        if (StringUtils.hasText(orderFieldName))
            sb.append(" ORDER BY ").append(orderFieldName).append(" DESC");
        int offset = page * size;
        sb.append(" OFFSET ").append(offset).append(" ROWS");
        sb.append(" FETCH NEXT ").append(size).append(" ROWS ONLY");

        return sb.toString();
    }

    public static StringBuilder appendFilters(String query, List<JdbcFilter> filters) {
        StringBuilder sb = new StringBuilder(query);
        filters.forEach(filter -> {
            sb.append(" AND (");
            sb.append(filter.getConditionQuery());
            sb.append(")");
        });
        return sb;
    }

}
