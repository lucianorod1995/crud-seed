package com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types;

import com.cuotaq.mscuotaq.commons.domain.filters.Filter;
import com.cuotaq.mscuotaq.commons.domain.filters.RangeFilter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JdbcRangeFilter implements JdbcFilter {

    private String field;
    private Object from;
    private Object to;
    private boolean fromIncluded;
    private boolean toIncluded;

    @Override
    public String getConditionQuery() {
        StringBuilder sb = new StringBuilder(field);
        if (from != null) {
            sb.append(fromIncluded ? " >= " : " > ");
            sb.append(from);
        }
        if (from != null && to != null) {
            sb.append(" AND ");
            sb.append(field);
        }
        if (to != null) {
            sb.append(toIncluded ? " <= " : " < ");
            sb.append(to);
        }
        return sb.toString();
    }

    @Override
    public JdbcFilter fromDomain(Filter domainFilter, Class<?> jdbcModelClass) {
        RangeFilter domain = (RangeFilter) domainFilter;
        Object formattedFrom = JdbcFilter.getFormatter(jdbcModelClass, domain.getKey()).format(domain.getFrom());
        Object formattedTo = JdbcFilter.getFormatter(jdbcModelClass, domain.getKey()).format(domain.getTo());

        return JdbcRangeFilter.builder()
                .field(domain.getKey())
                .from(formattedFrom)
                .to(formattedTo)
                .fromIncluded(domain.isFromIncluded())
                .toIncluded(domain.isToIncluded())
                .build();
    }
}
