package com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types;

import com.cuotaq.mscuotaq.commons.domain.filters.EqualsFilter;
import com.cuotaq.mscuotaq.commons.domain.filters.Filter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JdbcEqualsFilter implements JdbcFilter {

    private String field;
    private Object value;

    @Override
    public String getConditionQuery() {
        if (value == null || "NULL".equalsIgnoreCase(value.toString())) {
            return field + " IS NULL";
        }
        return field + " = " + value.toString();
    }

    @Override
    public JdbcFilter fromDomain(Filter domainFilter, Class<?> jdbcModelClass) {
        EqualsFilter domain = (EqualsFilter) domainFilter;
        Object formattedValue = JdbcFilter.getFormatter(jdbcModelClass, domain.getKey()).format(domain.getValue());
        return JdbcEqualsFilter.builder()
                .field(domain.getKey())
                .value(formattedValue)
                .build();
    }

}
