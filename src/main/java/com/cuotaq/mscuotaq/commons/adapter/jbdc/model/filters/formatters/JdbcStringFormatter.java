package com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.formatters;

public class JdbcStringFormatter implements JdbcFormatter {

    @Override
    public Object format(Object value) {
        if(value == null || "null".equalsIgnoreCase(value.toString())) {
            return value;
        }
        return "'" + value + "'";
    }
}
