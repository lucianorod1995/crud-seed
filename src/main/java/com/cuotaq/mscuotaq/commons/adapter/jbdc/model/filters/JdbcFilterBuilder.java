package com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters;

import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types.JdbcCompositeFilter;
import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types.JdbcContainsFilter;
import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types.JdbcEqualsFilter;
import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types.JdbcEqualsIgnoreCaseFilter;
import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types.JdbcFilter;
import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types.JdbcInFilter;
import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types.JdbcNotEqualsFilter;
import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types.JdbcRangeFilter;
import com.cuotaq.mscuotaq.commons.domain.filters.CompositeFilter;
import com.cuotaq.mscuotaq.commons.domain.filters.ContainsFilter;
import com.cuotaq.mscuotaq.commons.domain.filters.EqualsFilter;
import com.cuotaq.mscuotaq.commons.domain.filters.EqualsIgnoreCaseFilter;
import com.cuotaq.mscuotaq.commons.domain.filters.Filter;
import com.cuotaq.mscuotaq.commons.domain.filters.InListFilter;
import com.cuotaq.mscuotaq.commons.domain.filters.NotEqualsFilter;
import com.cuotaq.mscuotaq.commons.domain.filters.RangeFilter;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JdbcFilterBuilder {

    private final Map<Class<? extends Filter>, JdbcFilter> jdbcFilterFactory = Map.of(
            RangeFilter.class, new JdbcRangeFilter(),
            EqualsFilter.class, new JdbcEqualsFilter(),
            NotEqualsFilter.class, new JdbcNotEqualsFilter(),
            EqualsIgnoreCaseFilter.class, new JdbcEqualsIgnoreCaseFilter(),
            InListFilter.class, new JdbcInFilter(),
            CompositeFilter.class, new JdbcCompositeFilter(this),
            ContainsFilter.class, new JdbcContainsFilter()
    );

    public JdbcFilter build(Filter filter, Class<?> clazz) {
        return jdbcFilterFactory.get(filter.getClass())
                .fromDomain(filter, clazz);
    }

    public List<JdbcFilter> build(List<Filter> filters, Class<?> clazz) {
        return filters.stream()
                .map(filter -> this.build(filter, clazz))
                .collect(Collectors.toList());
    }
}
