package com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types;

import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.JdbcFilterBuilder;
import com.cuotaq.mscuotaq.commons.domain.filters.CompositeFilter;
import com.cuotaq.mscuotaq.commons.domain.filters.Filter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JdbcCompositeFilter implements JdbcFilter {
    private String connector;
    private JdbcFilter actual;
    private JdbcFilter next;
    private JdbcFilterBuilder builder;

    public JdbcCompositeFilter(JdbcFilterBuilder builder) {
        this.builder = builder;
    }

    @Override
    public String getConditionQuery() {
        return actual.getConditionQuery() + " " + connector + " " + next.getConditionQuery();
    }

    @Override
    public JdbcFilter fromDomain(Filter domainFilter, Class<?> clazz) {
        CompositeFilter domain = (CompositeFilter) domainFilter;
        return JdbcCompositeFilter.builder()
                .connector(domain.getConnector().name())
                .actual(builder.build(domain.getActual(), clazz))
                .next(builder.build(domain.getNext(), clazz))
                .build();
    }
}
