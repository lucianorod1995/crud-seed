package com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types;

import com.cuotaq.mscuotaq.commons.domain.filters.Filter;
import com.cuotaq.mscuotaq.commons.domain.filters.InListFilter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JdbcInFilter implements JdbcFilter {

    private String field;
    private List<Object> values;

    @Override
    public String getConditionQuery() {
        return field + " IN (" +
                values.stream().map(Object::toString).collect(Collectors.joining(",")) +
                ")";
    }

    @Override
    public JdbcFilter fromDomain(Filter domainFilter, Class<?> jdbcModelClass) {
        InListFilter domain = (InListFilter) domainFilter;
        List<Object> formattedValues = domain.getValues().stream()
                .map(value -> JdbcFilter.getFormatter(jdbcModelClass, domain.getKey()).format(value))
                .collect(Collectors.toList());
        return JdbcInFilter.builder()
                .field(domain.getKey())
                .values(formattedValues)
                .build();
    }

}
