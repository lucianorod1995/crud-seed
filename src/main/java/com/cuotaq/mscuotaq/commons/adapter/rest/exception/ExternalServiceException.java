package com.cuotaq.mscuotaq.commons.adapter.rest.exception;

public class ExternalServiceException extends RuntimeException {

    public ExternalServiceException() {
        super();
    }

    public ExternalServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
