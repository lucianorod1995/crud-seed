package com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.formatters;

public interface JdbcFormatter {

    Object format(Object value);
}
