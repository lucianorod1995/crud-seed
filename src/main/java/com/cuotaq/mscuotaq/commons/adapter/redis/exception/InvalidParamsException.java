package com.cuotaq.mscuotaq.commons.adapter.redis.exception;

public class InvalidParamsException extends RuntimeException{
    public InvalidParamsException(String message){super(message);}
}
