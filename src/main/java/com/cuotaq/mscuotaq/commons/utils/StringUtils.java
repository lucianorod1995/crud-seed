package com.cuotaq.mscuotaq.commons.utils;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class StringUtils extends org.springframework.util.StringUtils {

    public static String snakeToCamel(String snakeCase) {
        if (!hasText(snakeCase)) {
            return snakeCase;
        }
        StringBuilder camelCase = new StringBuilder();
        boolean capitalizeNext = false;

        for (char c : snakeCase.toCharArray()) {
            if (c == '_') {
                capitalizeNext = true;
            } else {
                if (capitalizeNext) {
                    camelCase.append(Character.toUpperCase(c));
                    capitalizeNext = false;
                } else {
                    camelCase.append(c);
                }
            }
        }
        return camelCase.toString();
    }

    public static String nullSafeConcat(CharSequence delimiter, String... strings) {
        return Arrays.stream(strings)
                .filter(Objects::nonNull)
                .collect(Collectors.joining(delimiter));
    }
}
