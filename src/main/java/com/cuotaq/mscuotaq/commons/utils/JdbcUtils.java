package com.cuotaq.mscuotaq.commons.utils;

import com.cuotaq.mscuotaq.commons.application.exception.BusinessException;
import com.cuotaq.mscuotaq.commons.config.ErrorCode;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.postgresql.util.PGobject;
import org.springframework.util.CollectionUtils;

import java.sql.SQLException;
import java.util.Map;

@Slf4j
public class JdbcUtils {

    public static Map<String, Object> convertToMap(PGobject pGobject, ObjectMapper objectMapper) {
        Map<String, Object> jsonMap = null;
        try {
            if (pGobject != null && pGobject.getValue() != null)
                jsonMap = objectMapper.readValue(pGobject.getValue(), new TypeReference<>() {
                });
        } catch (JsonProcessingException ex) {
            log.error("Error converting PGobject to Map", ex);
        }
        return jsonMap;
    }

    public static PGobject mapToPGObject(Map<String, Object> map, ObjectMapper objectMapper) {
        if (CollectionUtils.isEmpty(map)) {
            return null;
        }
        PGobject pgObject = new PGobject();
        pgObject.setType("jsonb");

        try {
            String json = objectMapper.writeValueAsString(map);
            pgObject.setValue(json);
        } catch (JsonProcessingException | SQLException e) {
            log.error("Error converting Map to PGobject", e);
            throw new BusinessException(ErrorCode.MAPPING_ERROR);
        }

        return pgObject;
    }
}
