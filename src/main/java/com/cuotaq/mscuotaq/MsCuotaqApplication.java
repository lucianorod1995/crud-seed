package com.cuotaq.mscuotaq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsCuotaqApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsCuotaqApplication.class, args);
    }

}
