package com.cuotaq.mscuotaq.companies.domain;

/*IMPORTS*/

import lombok.Builder;
import lombok.Value;
import lombok.With;



@Value
@Builder
public class Company {

    @With
    Integer companyId;
    String userId;
    String name;
}