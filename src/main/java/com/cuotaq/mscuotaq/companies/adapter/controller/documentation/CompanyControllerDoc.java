package com.cuotaq.mscuotaq.companies.adapter.controller.documentation;

/*IMPORTS*/
import com.cuotaq.mscuotaq.companies.adapter.controller.model.CompanyRestModel;
import com.cuotaq.mscuotaq.companies.adapter.controller.model.CreateCompanyRequestBody;
import com.cuotaq.mscuotaq.companies.adapter.controller.model.PatchCompanyRequestBody;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface CompanyControllerDoc {
    @Operation(summary = "Get Company by Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Company found"),
            @ApiResponse(responseCode = "400", description = "Invalid id", content = @Content),
            @ApiResponse(responseCode = "401", description = "Invalid token", content = @Content),
            @ApiResponse(responseCode = "403", description = "Permission denied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Company not found", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content),
            @ApiResponse(responseCode = "503", description = "Service unavailable", content = @Content)
    })
    CompanyRestModel getCompany(Integer companyId);

    @Operation(summary = "Create Company")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Company created"),
            @ApiResponse(responseCode = "400", description = "Malformed request syntax", content = @Content),
            @ApiResponse(responseCode = "401", description = "Invalid token", content = @Content),
            @ApiResponse(responseCode = "403", description = "Permission denied", content = @Content),
            @ApiResponse(responseCode = "409", description = "Company already exists", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content),
            @ApiResponse(responseCode = "503", description = "Service unavailable", content = @Content)
    })
    ResponseEntity<CompanyRestModel> createCompany(CreateCompanyRequestBody body);

    @Operation(summary = "Get Companies by filters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Request processed correctly"),
            @ApiResponse(responseCode = "400", description = "Malformed request syntax", content = @Content),
            @ApiResponse(responseCode = "401", description = "Invalid token", content = @Content),
            @ApiResponse(responseCode = "403", description = "Permission denied", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content),
            @ApiResponse(responseCode = "503", description = "Service unavailable", content = @Content)
    })
    ResponseEntity getCompanies(Integer page, Integer size, Map<String, String> queryParams);

    @Operation(summary = "Patch Company with non null values")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Company patched"),
            @ApiResponse(responseCode = "400", description = "Malformed request syntax", content = @Content),
            @ApiResponse(responseCode = "401", description = "Invalid token", content = @Content),
            @ApiResponse(responseCode = "403", description = "Permission denied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Company not found", content = @Content),
            @ApiResponse(responseCode = "409", description = "Company already exists", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content),
            @ApiResponse(responseCode = "503", description = "Service unavailable", content = @Content)
    })
    CompanyRestModel patchCompany(Integer companyId, PatchCompanyRequestBody body);

    @Operation(summary = "Delete Company")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Company deleted"),
            @ApiResponse(responseCode = "401", description = "Invalid token", content = @Content),
            @ApiResponse(responseCode = "403", description = "Permission denied", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content),
            @ApiResponse(responseCode = "503", description = "Service unavailable", content = @Content)
    })
    ResponseEntity<Void> deleteCompany(Integer companyId);
}
