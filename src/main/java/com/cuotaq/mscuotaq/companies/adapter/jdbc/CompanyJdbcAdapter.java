package com.cuotaq.mscuotaq.companies.adapter.jdbc;

/*IMPORTS*/
import com.cuotaq.mscuotaq.commons.adapter.jbdc.SqlReader;
import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.JdbcFilterBuilder;
import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.JdbcQueryBuilder;
import com.cuotaq.mscuotaq.commons.adapter.jbdc.model.filters.types.JdbcFilter;
import com.cuotaq.mscuotaq.commons.application.exception.EntityConflictException;
import com.cuotaq.mscuotaq.commons.application.exception.EntityNotFoundException;
import com.cuotaq.mscuotaq.commons.application.exception.RepositoryNotAvailableException;
import com.cuotaq.mscuotaq.commons.domain.Page;
import com.cuotaq.mscuotaq.commons.domain.filters.Filter;
import com.cuotaq.mscuotaq.companies.adapter.jdbc.model.CompanyJdbcModel;
import com.cuotaq.mscuotaq.companies.application.port.out.CompanyRepository;
import com.cuotaq.mscuotaq.companies.config.ErrorCode;
import com.cuotaq.mscuotaq.companies.domain.Company;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Component
@Slf4j
@AllArgsConstructor
public class CompanyJdbcAdapter implements CompanyRepository {

    private static final String getCompaniesQuery = SqlReader.readSql("companies/sql/getCompanies.sql");
    private static final String insertCompaniesQuery = SqlReader.readSql("companies/sql/insertCompany.sql");
    private static final String getCompanyByIdQuery = SqlReader.readSql("companies/sql/getCompanyById.sql");
    private static final String patchCompanyQuery = SqlReader.readSql("companies/sql/patchCompany.sql");
    private static final String deleteCompanyQuery = SqlReader.readSql("companies/sql/deleteCompany.sql");
    private static final String COMPANY_ID = "COMPANY_ID";
    private static final String USER_ID = "USER_ID";
    private static final String NAME = "NAME";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public Company save(Company company) {
        log.info("Saving Company {}", company);
        Map<String, Object> params = new HashMap<>();
        params.put(USER_ID, company.getUserId());
        params.put(NAME, company.getName());

        printQuery(insertCompaniesQuery, params);
        Integer companyId = handleExceptions(() -> jdbcTemplate.queryForObject(
                insertCompaniesQuery, params, Integer.class)
        );

        log.info("Saved Company with id {}", companyId);
        return company.withCompanyId(companyId);
    }

    @Override
    public Company findById(Integer companyId) {
        log.info("Finding Company with id {}", companyId);
        final Map<String, Object> params = new HashMap<>();
        params.put(COMPANY_ID, companyId);

        printQuery(getCompanyByIdQuery, params);
        CompanyJdbcModel companyResult = handleExceptions(() -> jdbcTemplate.queryForObject(
                getCompanyByIdQuery, params, new BeanPropertyRowMapper<>(CompanyJdbcModel.class))
        );
        log.info("Found Company {}", companyResult);
        return companyResult.toDomain();
    }

    @Override
    public Page<Company> findAll(Integer page, Integer size, List<Filter> filters) {
        log.info("Finding Companies with page {} size {} filters {}", page, size, filters);
        List<JdbcFilter> jdbcFilters = new JdbcFilterBuilder().build(filters, CompanyJdbcModel.class);

        String countQuery = JdbcQueryBuilder.buildCountQuery(getCompaniesQuery, jdbcFilters);
        printQuery(countQuery, Collections.emptyMap());
        int totalElements = handleExceptions(() -> jdbcTemplate.queryForObject(
                countQuery, Collections.emptyMap(), Integer.class)
        );
        int totalPages = totalElements == 0 ? 0 : 1 + (totalElements / size);

        String pageQuery = JdbcQueryBuilder.buildPagedSelectQuery(
                getCompaniesQuery, "created_at", jdbcFilters, page, size
        );
        printQuery(pageQuery, Collections.emptyMap());
        List<CompanyJdbcModel> companiesResult = handleExceptions(() -> jdbcTemplate.query(
                pageQuery, Collections.emptyMap(), new BeanPropertyRowMapper<>(CompanyJdbcModel.class))
        );

        log.debug("Found Companies {} with page {} size {} filters {}", companiesResult, page, size, filters);
        return Page.<Company>builder()
                .content(companiesResult.stream()
                        .map(CompanyJdbcModel::toDomain)
                        .collect(Collectors.toList()))
                .size(size)
                .number(page)
                .totalElements(totalElements)
                .totalPages(totalPages)
                .build();
    }

    @Override
    public List<Company> findAll(List<Filter> filters) {
        log.info("Finding Companies with filters {}", filters);
        List<JdbcFilter> jdbcFilters = new JdbcFilterBuilder().build(filters, CompanyJdbcModel.class);

        String selectQuery = JdbcQueryBuilder.buildSelectQuery(getCompaniesQuery, jdbcFilters);
        printQuery(selectQuery, Collections.emptyMap());
        List<CompanyJdbcModel> companiesResult = handleExceptions(() -> jdbcTemplate.query(
                selectQuery, Collections.emptyMap(), new BeanPropertyRowMapper<>(CompanyJdbcModel.class))
        );

        log.debug("Found Companies {} filters {}", companiesResult, filters);
        return companiesResult.stream()
                .map(CompanyJdbcModel::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public Company patch(Company company) {
        log.info("Patching Company with {}", company);
        final Map<String, Object> params = new HashMap<>();
        params.put(COMPANY_ID, company.getCompanyId());
        params.put(USER_ID, company.getUserId());
        params.put(NAME, company.getName());

        printQuery(patchCompanyQuery, params);
        CompanyJdbcModel companyResult = handleExceptions(() -> jdbcTemplate.queryForObject(
                patchCompanyQuery, params, new BeanPropertyRowMapper<>(CompanyJdbcModel.class))
        );
        log.info("Patched Company {}", companyResult);
        return companyResult.toDomain();
    }

    @Override
    public void deleteById(Integer companyId) {
        log.info("Deleting Company with id {}", companyId);
        final Map<String, Object> params = new HashMap<>();
        params.put(COMPANY_ID, companyId);

        printQuery(deleteCompanyQuery, params);
        handleExceptions(() -> jdbcTemplate.update(deleteCompanyQuery, params));
        log.info("Deleted Company with id {}", companyId);
    }

    private void printQuery(String query, Map<String, Object> queryParams) {
        log.debug("Executing query {} with params {}", query, queryParams);
    }

    private <T> T handleExceptions(Supplier<T> supplier) {
        try {
            return supplier.get();
        } catch (EmptyResultDataAccessException ex) {
            log.error("Error finding Company", ex);
            throw new EntityNotFoundException(ErrorCode.COMPANY_NOT_FOUND);
        } catch (DataIntegrityViolationException ex) {
            log.error("Error saving Company", ex);
            throw new EntityConflictException(ErrorCode.COMPANY_CONFLICT);
        } catch (DataAccessException ex) {
            log.error("Unexpected error saving Company", ex);
            throw new RepositoryNotAvailableException(ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE);
        } catch (Exception ex) {
            log.error("Unexpected error saving Company", ex);
            throw new RuntimeException("Unexpected error");
        }
    }
}
