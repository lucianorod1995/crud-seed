package com.cuotaq.mscuotaq.companies.adapter.controller;

/*IMPORTS*/
import com.cuotaq.mscuotaq.commons.adapter.controller.model.PageResponse;
import com.cuotaq.mscuotaq.commons.domain.Page;
import com.cuotaq.mscuotaq.companies.adapter.controller.documentation.CompanyControllerDoc;
import com.cuotaq.mscuotaq.companies.adapter.controller.model.CompanyRestModel;
import com.cuotaq.mscuotaq.companies.adapter.controller.model.CreateCompanyRequestBody;
import com.cuotaq.mscuotaq.companies.adapter.controller.model.PatchCompanyRequestBody;
import com.cuotaq.mscuotaq.companies.application.port.in.CreateCompanyCommand;
import com.cuotaq.mscuotaq.companies.application.port.in.DeleteCompanyCommand;
import com.cuotaq.mscuotaq.companies.application.port.in.GetCompaniesQuery;
import com.cuotaq.mscuotaq.companies.application.port.in.GetCompanyQuery;
import com.cuotaq.mscuotaq.companies.application.port.in.PatchCompanyCommand;
import com.cuotaq.mscuotaq.companies.domain.Company;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/api/companies")
public class CompanyController implements CompanyControllerDoc {

    private final GetCompanyQuery getCompanyQuery;
    private final CreateCompanyCommand createCompanyCommand;
    private final GetCompaniesQuery getCompaniesQuery;
    private final PatchCompanyCommand patchCompanyCommand;
    private final DeleteCompanyCommand deleteCompanyCommand;

    private static final Set<String> nonFilterKeys = Set.of("page", "size");

    @GetMapping("/{company_id}")
    public CompanyRestModel getCompany(@PathVariable("company_id") Integer companyId) {
        log.info("Getting company with companyId {}", companyId);
        GetCompanyQuery.Data data = GetCompanyQuery.Data.builder()
                .companyId(companyId)
                .build();
        CompanyRestModel companyResult = CompanyRestModel.fromDomain(getCompanyQuery.execute(data));
        log.info("Get company with companyId {} replied {}", companyId, companyResult);
        return companyResult;
    }

    @PostMapping
    public ResponseEntity<CompanyRestModel> createCompany(@RequestBody @Valid CreateCompanyRequestBody body) {
        log.info("Creating company {}", body);
        CreateCompanyCommand.Data data = CreateCompanyCommand.Data.builder()
                .userId(body.getUserId())
                .name(body.getName())
                .build();
        CompanyRestModel companyResult = CompanyRestModel.fromDomain(createCompanyCommand.execute(data));
        log.info("Created company {}", companyResult);
        return ResponseEntity.status(HttpStatus.CREATED).body(companyResult);
    }

    @GetMapping
    public ResponseEntity getCompanies(
            @RequestParam(name = "page", required = false) Integer page,
            @RequestParam(name = "size", required = false) Integer size,
            @RequestParam Map<String, String> queryParams
    ) {
        ResponseEntity response;
        Map<String, String> filtersMap = new HashMap<>(queryParams);
        filtersMap.keySet().removeAll(nonFilterKeys);
        GetCompaniesQuery.Data data = GetCompaniesQuery.Data.builder()
                .filtersMap(filtersMap)
                .build();
        if (page != null && size != null) {
            log.info("Getting companies page with filters {}", queryParams);
            Page<Company> companies = getCompaniesQuery.execute(data, page, size);
            PageResponse<CompanyRestModel> companiesPageResponse = new PageResponse<CompanyRestModel>()
                    .fromDomain(companies, CompanyRestModel::fromDomain);
            response = ResponseEntity.ok(companiesPageResponse);
        } else {
            log.info("Getting companies list with filters {}", queryParams);
            List<Company> companies = getCompaniesQuery.execute(data);
            response = ResponseEntity.ok(CompanyRestModel.fromListDomain(companies));
        }

        log.info("Get companies by filters {} replied {}", queryParams, response.getBody());
        return response;
    }

    @PatchMapping("/{company_id}")
    public CompanyRestModel patchCompany(
            @PathVariable("company_id") Integer companyId,
            @RequestBody @Valid PatchCompanyRequestBody body
    ) {
        log.info("Patching company with companyId {} with body {}", companyId, body);
        PatchCompanyCommand.Data data = PatchCompanyCommand.Data.builder()
                .companyId(companyId)
                .userId(body.getUserId())
                .name(body.getName())
                .build();
        CompanyRestModel companyResult = CompanyRestModel.fromDomain(patchCompanyCommand.execute(data));
        log.debug("Company with companyId {} updated with {}", companyId, companyResult);
        return companyResult;
    }

    @DeleteMapping("/{company_id}")
    public ResponseEntity<Void> deleteCompany(@PathVariable("company_id") Integer companyId) {
        log.info("Deleting company with companyId {}", companyId);
        DeleteCompanyCommand.Data data = DeleteCompanyCommand.Data.builder()
                .companyId(companyId)
                .build();
        deleteCompanyCommand.execute(data);
        log.info("Company with companyId {} deleted", companyId);
        return ResponseEntity.noContent().build();
    }
}
