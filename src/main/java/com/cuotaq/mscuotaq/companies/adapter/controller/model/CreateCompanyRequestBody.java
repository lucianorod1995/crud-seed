package com.cuotaq.mscuotaq.companies.adapter.controller.model;

import com.cuotaq.mscuotaq.companies.config.ErrorCode;
/*IMPORTS*/
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@AllArgsConstructor
@NoArgsConstructor
public class CreateCompanyRequestBody {

    @NotNull(message = ErrorCode.Constants.INVALID_USER_ID)
    private String userId;
    @NotNull(message = ErrorCode.Constants.INVALID_NAME)
    private String name;
}
