package com.cuotaq.mscuotaq.companies.config;

import com.cuotaq.mscuotaq.commons.config.IErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ErrorCode implements IErrorCode {

    COMPANY_NOT_FOUND(Constants.COMPANY_NOT_FOUND),
    COMPANY_CONFLICT(Constants.COMPANY_CONFLICT),
    COMPANY_REPOSITORY_NOT_AVAILABLE(Constants.COMPANY_REPOSITORY_NOT_AVAILABLE),
    COMPANY_ALREADY_EXISTS(Constants.COMPANY_ALREADY_EXISTS),
    INVALID_USER_ID(Constants.INVALID_USER_ID),
    INVALID_NAME(Constants.INVALID_NAME);

    private final String value;

    public static class Constants {
        public static final String COMPANY_NOT_FOUND = "COMPANY_NOT_FOUND";
        public static final String COMPANY_CONFLICT = "COMPANY_CONFLICT";
        public static final String COMPANY_REPOSITORY_NOT_AVAILABLE = "COMPANY_REPOSITORY_NOT_AVAILABLE";
        public static final String COMPANY_ALREADY_EXISTS = "COMPANY_ALREADY_EXISTS";
        public static final String INVALID_USER_ID = "INVALID_USER_ID";
        public static final String INVALID_NAME = "INVALID_NAME";
    }
}
