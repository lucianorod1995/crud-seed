package com.cuotaq.mscuotaq.companies.application.port.out;

import com.cuotaq.mscuotaq.commons.domain.Page;
import com.cuotaq.mscuotaq.commons.domain.filters.Filter;
import com.cuotaq.mscuotaq.companies.domain.Company;

import java.util.List;

public interface CompanyRepository {

    Company save(Company company);

    Company findById(Integer companyId);

    List<Company> findAll(List<Filter> filters);

    Page<Company> findAll(Integer page, Integer size, List<Filter> filters);

    Company patch(Company company);

    void deleteById(Integer companyId);
}
