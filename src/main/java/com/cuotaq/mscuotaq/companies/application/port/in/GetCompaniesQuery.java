package com.cuotaq.mscuotaq.companies.application.port.in;

import com.cuotaq.mscuotaq.commons.domain.Page;
import com.cuotaq.mscuotaq.companies.domain.Company;
import lombok.Builder;
import lombok.Value;

import java.util.List;
import java.util.Map;

public interface GetCompaniesQuery {

    Page<Company> execute(Data data, Integer page, Integer size);

    List<Company> execute(Data data);

    @Value
    @Builder
    class Data {
        Map<String, String> filtersMap;
    }
}
