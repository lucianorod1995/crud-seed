package com.cuotaq.mscuotaq.companies.application.usecase;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import com.cuotaq.mscuotaq.companies.application.port.in.GetCompanyQuery;
import com.cuotaq.mscuotaq.companies.application.port.out.CompanyRepository;
import com.cuotaq.mscuotaq.companies.domain.Company;

@Slf4j
@Component
@AllArgsConstructor
public class GetCompanyUseCase implements GetCompanyQuery {

    private final CompanyRepository companyRepository;

    @Override
    public Company execute(Data data) {
        log.info("Executing get Company use case with data {}", data);
        Company companyResult = companyRepository.findById(data.getCompanyId());
        log.info("Company Repository replied {}", companyResult);
        return companyResult;
    }
}
