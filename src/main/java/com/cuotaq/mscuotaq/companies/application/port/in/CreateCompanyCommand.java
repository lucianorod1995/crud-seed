package com.cuotaq.mscuotaq.companies.application.port.in;

import com.cuotaq.mscuotaq.companies.domain.Company;
/*IMPORTS*/
import lombok.Builder;
import lombok.Value;



public interface CreateCompanyCommand {

    Company execute(Data data);

    @Value
    @Builder
    class Data {
        String userId;
        String name;

        public Company toDomain() {
            return Company.builder()
                    .userId(userId)
                    .name(name)
                    .build();
        }
    }
}

