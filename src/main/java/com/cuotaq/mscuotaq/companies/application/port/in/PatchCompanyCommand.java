package com.cuotaq.mscuotaq.companies.application.port.in;

import com.cuotaq.mscuotaq.companies.domain.Company;
/*IMPORTS*/
import lombok.Builder;
import lombok.Value;



public interface PatchCompanyCommand {

    Company execute(Data data);

    @Value
    @Builder
    class Data {
        Integer companyId;
        String userId;
        String name;

        public Company toDomain() {
            return Company.builder()
                    .companyId(companyId)
                    .userId(userId)
                    .name(name)
                    .build();
        }
    }
}
