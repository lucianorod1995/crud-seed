package com.cuotaq.mscuotaq.companies.application.port.in;

import com.cuotaq.mscuotaq.companies.domain.Company;
import lombok.Builder;
import lombok.Value;

public interface GetCompanyQuery {

    Company execute(Data data);

    @Value
    @Builder
    class Data {
        Integer companyId;
    }
}
