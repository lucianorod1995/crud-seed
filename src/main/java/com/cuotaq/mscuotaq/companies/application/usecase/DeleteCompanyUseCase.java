package com.cuotaq.mscuotaq.companies.application.usecase;

import com.cuotaq.mscuotaq.commons.application.exception.EntityNotFoundException;
import com.cuotaq.mscuotaq.companies.application.port.in.DeleteCompanyCommand;
import com.cuotaq.mscuotaq.companies.application.port.out.CompanyRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class DeleteCompanyUseCase implements DeleteCompanyCommand {

    private final CompanyRepository companyRepository;

    @Override
    public void execute(Data data) {
        try {
            log.info("Executing delete Company use case with data {}", data);
            companyRepository.deleteById(data.getCompanyId());
        } catch (EntityNotFoundException ignored) {
        }
        log.info("Finished executing delete Company use case");
    }
}
