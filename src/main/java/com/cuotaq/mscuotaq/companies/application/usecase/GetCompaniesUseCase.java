package com.cuotaq.mscuotaq.companies.application.usecase;

import com.cuotaq.mscuotaq.commons.domain.Page;
import com.cuotaq.mscuotaq.commons.domain.filters.Filter;
import com.cuotaq.mscuotaq.commons.domain.filters.FilterBuilder;
import com.cuotaq.mscuotaq.companies.application.port.in.GetCompaniesQuery;
import com.cuotaq.mscuotaq.companies.application.port.out.CompanyRepository;
import com.cuotaq.mscuotaq.companies.domain.Company;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
@AllArgsConstructor
public class GetCompaniesUseCase implements GetCompaniesQuery {

    private final CompanyRepository companyRepository;

    public Page<Company> execute(Data data, Integer page, Integer size) {
        log.info("Executing get Companies use case with data {}, page {}, size {}", data, page, size);
        List<Filter> filters = new FilterBuilder().build(data.getFiltersMap());
        Page<Company> companiesResult = companyRepository.findAll(page, size, filters);
        log.debug("Company Repository replied {}", companiesResult);
        return companiesResult;
    }

    public List<Company> execute(Data data) {
        log.info("Executing get Companies list use case with data {}", data);
        List<Filter> filters = new FilterBuilder().build(data.getFiltersMap());
        List<Company> companiesResult = companyRepository.findAll(filters);
        log.debug("Company Repository replied {}", companiesResult);
        return companiesResult;
    }
}
