package com.cuotaq.mscuotaq.companies.application.usecase;

import com.cuotaq.mscuotaq.companies.application.port.in.CreateCompanyCommand;
import com.cuotaq.mscuotaq.companies.application.port.out.CompanyRepository;
import com.cuotaq.mscuotaq.companies.domain.Company;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class CreateCompanyUseCase implements CreateCompanyCommand {

    private final CompanyRepository companyRepository;

    @Override
    public Company execute(Data data) {
        log.info("Executing create Company use case with data {}", data);
        Company companyResult = companyRepository.save(data.toDomain());
        log.info("Company Repository replied {}", companyResult);
        return companyResult;
    }
}
