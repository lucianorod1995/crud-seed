UPDATE companies SET
    user_id = COALESCE(:USER_ID, user_id),
    name = COALESCE(:NAME, name)
WHERE company_id = :COMPANY_ID
RETURNING *